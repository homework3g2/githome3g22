const express = require('express');
const body = require('body-parser')
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection');
const app = express();

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(body.urlencoded(express.static('public')));
app.use(cookie());
app.use(session({ secret: 'Passw0rd' }));
app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: '123456',
    port: 3306,
    database: 'gitlabhw3'
}, 'single'));

const s611998011Route = require('./routes/s611998011Route');
app.use('/', s611998011Route);

const tiewRoute = require('./routes/s611998019Route');
app.use('/', tiewRoute);

const fordRoute = require('./routes/S611998030Route');
app.use('/', fordRoute);

app.listen('8081','127.0.0.1');
