const express = require('express');
const router = express.Router();

const s611998011Controller = require('../controllers/s611998011Controller');
const validator11 = require('../controllers/validator11');

router.get('/s611998011', s611998011Controller.list);
router.post('/s611998011/add', validator11.addValidator, s611998011Controller.add);
router.get('/s611998011/delete/:id', s611998011Controller.delete);
router.get('/s611998011/del/:id',s611998011Controller.del);
router.get('/s611998011/update/:id', s611998011Controller.edit);
router.post('/s611998011/up/', validator11.editValidator, s611998011Controller.up);
router.get('/s611998011/new', s611998011Controller.new);

module.exports = router;
