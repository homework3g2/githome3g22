const express = require('express');
const router = express.Router();

const fordController = require('../controllers/s611998030Controller');
const homeController = require('../controllers/homeController');
const validator = require('../controllers/s30Validator')

router.get('/home',homeController.home);
router.get('/ford',fordController.list);
router.post('/ford/add',validator.addValidator,fordController.save);
router.get('/ford/deleteNow/:id',fordController.deleteNow);
router.get('/ford/delete/:id',fordController.delete);
router.get('/ford/update/:id',validator.editValidator,fordController.edit);
router.post('/ford/update/:id',validator.editValidator,fordController.update);
router.get('/ford/new',fordController.new);

module.exports = router;
