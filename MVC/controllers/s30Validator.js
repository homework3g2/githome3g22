const { check } = require('express-validator');

exports.addValidator = [check('z611998030',"เลขไม่ถูกต้อง!").isFloat(),
check('y611998030',"ชื่อไม่ถูกต้อง").not().isEmpty(),
check('x611998030',"เลขไม่ถูกต้อง ").isInt(),
check('w611998030',"วันเดือนปีไม่ถูกต้อง").isDate(),
check('v611998030',"เวลาไม่ถูกต้อง").not().isEmpty(),];

exports.editValidator = [check('z611998030',"เลขไม่ถูกต้อง!").isFloat(),
check('y611998030',"ชื่อไม่ถูกต้อง").not().isEmpty(),
check('x611998030',"เลขไม่ถูกต้อง").isInt(),
check('w611998030',"วันเดือนปีไม่ถูกต้อง").isDate(),
check('v611998030',"เวลาไม่ถูกต้อง").not().isEmpty(),];
