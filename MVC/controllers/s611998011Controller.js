
const controller = {};
const { validationResult } = require('express-validator');


controller.list = (req,res) => {
  req.getConnection((err,conn) => {
    conn.query("SELECT * FROM mirot611998011", (err, s611998011s) => {

      if(err){
        res.json(err);
      }
      res.render('s611998011/s611998011',{
        data:s611998011s,
        session: req.session
      });
    });
  });
};

controller.add = (req,res) => {
  const data=req.body;
  const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/s611998011/new');
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('INSERT INTO mirot611998011 set ?',[data],(err,s611998011s) =>{
          if(err){
            res.json(err);
          }

          console.log(s611998011s);
          res.redirect('/s611998011');
        });
      });
    }
};

controller.save =(req,res) =>{
  console.log(req.body);
  const data=req.body;
  req.getConnection((err,conn) =>{
    conn.query('INSERT INTO mirot611998011 set ?',[data],(err,s611998011s) =>{
      if(err){
        res.json(err);
      }

      console.log(s611998011s);
      res.redirect('/s611998011');
    });
  });
};


controller.delete =(req,res) =>{
  const {id} =req.params;
  req.getConnection((err,conn) =>{
    conn.query('DELETE FROM mirot611998011 WHERE id11 = ?',[id],(err,s611998011s) =>{
      if(err){
        res.json(err);
      }
      console.log(s611998011s);
      res.redirect('/s611998011');
    });
  });
};

controller.del =(req,res) =>{
  const {id} =req.params;
  req.getConnection((err,conn) =>{
    conn.query('DELETE FROM mirot611998011 WHERE id11 = ?',[id],(err,s611998011s) =>{
      if(err){
        res.json(err);
      }
      console.log(s611998011s);

      res.render('s611998011/s611998011FormDelete',{
              data:s611998011s[0],
              session: req.session

      });
    });
  });
};



controller.edit =(req,res) =>{
  const {id} =req.params;
  req.getConnection((err,conn) =>{
    conn.query('SELECT *  FROM (SELECT * FROM s611998011) as x JOIN (SELECT DATE_FORMAT(w611998011,"%Y-%m-%d") as time FROM mirot611998011) as y GROUP BY id11 HAVING id11 = ?',[id],(err,s611998011s) =>{
      if(err){
        res.json(err);
      }
      console.log(s611998011s);
      res.render('s611998011/s611998011Form',{
              data:s611998011s[0],
              session: req.session
      });
    });
  });
};

controller.up = (req,res) => {
  const errors = validationResult(req);
  const {id} =req.params;
  const data=req.body;
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM mirot611998011',[id],(err,s611998011s) =>{
          if(err){
            res.json(err);
          }
          console.log(s611998011s);
          res.render('s611998011/s611998011Form',{
                  data:s611998011s[0],
                  session: req.session
          });
        });
      });
    }else {
      req.session.success=true;
      req.session.topic="เพิ่มข้อมูลสำเร็จ";
      req.getConnection((err,conn) =>{
        conn.query('UPDATE mirot611998011 SET ? WHERE id11 = ?',[data,id],(err,s611998011s) =>{
          if(err){
            res.json(err);
          }
          console.log(s611998011s);
          res.redirect('/s611998011');
        });
      });
    }
};


controller.new =(req,res) =>{
  const data=null;
  res.render('s611998011/s611998011Form',{
    data:data,
    session: req.session
  });
};


module.exports = controller;
