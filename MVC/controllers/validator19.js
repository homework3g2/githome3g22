const { check } = require('express-validator');//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator

exports.addvalidator = [check('z611998019',"z19-float_ไม่ถูกต้อง").not().isEmpty(),
                        check('y611998019',"y19-varchar_ไม่ถูกต้อง").not().isEmpty(),
                        check('x611998019',"x19-int_ไม่ถูกต้อง").not().isEmpty(),
                        check('w611998019',"w19-date_ไม่ถูกต้อง").not().isEmpty(),
                        check('v611998019',"v19-time_ไม่ถูกต้อง").not().isEmpty()
                      ];

exports.editvalidator = [check('z611998019',"z19-float_ไม่ถูกต้อง").not().isEmpty(),
                        check('y611998019',"y19-varchar_ไม่ถูกต้อง").not().isEmpty(),
                        check('x611998019',"x19-int_ไม่ถูกต้อง").not().isEmpty(),
                        check('w611998019',"w19-date_ไม่ถูกต้อง").not().isEmpty(),
                        check('v611998019',"v19-time_ไม่ถูกต้อง").not().isEmpty()
                      ];
