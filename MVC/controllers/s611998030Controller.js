const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req,res) => {
  req.getConnection((err,conn) => {
    conn.query("SELECT * FROM mirot611998030 ",(err,s611998030)=>{
      if(err){
        res.json(err);
      }
      res.render('../views/s611998030/listForm',{
        session: req.session,
        data:s611998030
      });
    });
  });
};
controller.save = (req,res) => {
  const data = req.body;
  const errors = validationResult(req);
  req.getConnection((err,conn) => {
    conn.query('insert into mirot611998030 set ?',[data],(err,s611998030)=>{
      if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success=false;
        res.redirect('/ford/new')
      }else {
        req.session.success=true;
        req.session.topic="เพิ่มข้อมูลสำเร็จ";
        res.redirect('/ford')
      }
    });
  });
};


controller.delete = (req,res) => {
  const { id } = req.params;
  req.getConnection((err,conn) =>{
    conn.query('select * from mirot611998030 where id30 = ?',[id], (err,s611998030) => {
      if(err){
        res.json(err);
      }
      res.render('../views/s611998030/deleteForm',{
        session: req.session,
        data:s611998030[0]
    });
  });
});
};

controller.deleteNow = (req,res) => {
  const {id} = req.params;
  req.getConnection((err,conn) =>{
    conn.query('DELETE FROM mirot611998030 WHERE id30 = ?',[id], (err,s611998030) => {
      if(err){
        res.json(err);
      }
      console.log('../views/s611998030/listForm');
      res.redirect('/ford');
    });
  });
};

controller.edit = (req,res) => {
  const {id} = req.params;
  req.getConnection((err,conn) =>{
    conn.query('select * from mirot611998030 where id30 = ?',[id],(err,s611998030) =>{
      if(err){
        res.json(err);
      }
      res.render('../views/s611998030/editForm',{
        session: req.session,
        data:s611998030[0]
      });
    });
  });
};
controller.update = (req,res) => {
  const {id} = req.params;
  const data = req.body;
  const errors = validationResult(req);
      if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success=false;
        req.getConnection((err,conn) =>{
          conn.query('select * from mirot611998030 where id30 = ?',[id],(err,s611998030) =>{
            if(err){
              res.json(err);
            }
            res.render('../views/s611998030/editForm',{
              session: req.session,
              data:s611998030[0]
            });
          });
        });
      }else {
        req.session.success=true;
        req.session.topic="เพิ่มข้อมูลสำเร็จ";
        req.getConnection((err,conn) =>{
          conn.query('update mirot611998030 set ? where id30 = ?',[data,id],(err,s611998030)=> {
            if(err){
              res.json(err);
            }
          res.redirect('/ford')
          });
        });
      }

};
controller.new = (req,res)=>{
  const data = null;
  res.render('../views/s611998030/editForm',{
    session: req.session,
    data:data
  });
};

module.exports =controller;
